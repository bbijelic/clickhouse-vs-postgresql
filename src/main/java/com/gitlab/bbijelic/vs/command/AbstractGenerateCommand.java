package com.gitlab.bbijelic.vs.command;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.Random;

@Slf4j
public abstract class AbstractGenerateCommand {

    protected void generate(
            final String jdbcUrl,
            final String username,
            final String password,
            final String table,
            final Integer records) {

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            createTable(connection, table);
            insert(connection, table, records);
            log.info("Inserted {} records into the database.", records);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    protected abstract String getTableDdl(final String tableName);

    private void createTable(Connection connection, String tableName) throws SQLException {
        String createTableSQL = getTableDdl(tableName);

        try (PreparedStatement statement = connection.prepareStatement(createTableSQL)) {
            statement.execute();
        }
    }

    protected void insert(Connection connection, String tableName, int numRecords) throws SQLException {
        String insertSQL = String.format(
                "INSERT INTO %s (timestamp, category, value) VALUES (?, ?, ?)", tableName);

        try (PreparedStatement statement = connection.prepareStatement(insertSQL)) {
            Random random = new Random();

            for (int i = 0; i < numRecords; i++) {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                String category = "Category_" + (random.nextInt(10) + 1);
                double value = random.nextDouble() * 100;

                statement.setTimestamp(1, timestamp);
                statement.setString(2, category);
                statement.setDouble(3, value);

                statement.addBatch();

                if (i % 1000 == 0) {
                    statement.executeBatch();
                }
            }

            statement.executeBatch(); // Execute any remaining inserts
        }
    }

}
