package com.gitlab.bbijelic.vs.command.clickhouse;

import com.gitlab.bbijelic.vs.command.AbstractGenerateCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@Slf4j
@ShellComponent
@RequiredArgsConstructor
public class GenerateClickhouse extends AbstractGenerateCommand {

    @ShellMethod(
            key = "generate-clickhouse",
            value = "Generate ClickHouse data")
    public void generate(
            final @ShellOption(help = "JDBC connection URL", defaultValue = "jdbc:clickhouse://localhost:8123/category") String jdbcUrl,
            final @ShellOption(help = "JDBC connection username", defaultValue = "clickhouse") String username,
            final @ShellOption(help = "JDBC connection password", defaultValue = "clickhouse") String password,
            final @ShellOption(help = "Table name", defaultValue = "categories") String table,
            final @ShellOption(help = "Number of records to insert", defaultValue = "1000000") Integer records) {
        super.generate(jdbcUrl, username, password, table, records);
    }

    @Override
    protected String getTableDdl(String tableName) {
        return String.format(
                "CREATE TABLE IF NOT EXISTS %s (id UUID DEFAULT generateUUIDv4(), timestamp DateTime DEFAULT now(), category String, value Float64) ENGINE = MergeTree() PARTITION BY toYYYYMMDD(timestamp) ORDER BY (timestamp, category)",
                tableName);
    }

}
