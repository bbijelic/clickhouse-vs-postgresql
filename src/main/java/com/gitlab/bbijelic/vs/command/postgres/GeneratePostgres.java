package com.gitlab.bbijelic.vs.command.postgres;

import com.gitlab.bbijelic.vs.command.AbstractGenerateCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@Slf4j
@ShellComponent
@RequiredArgsConstructor
public class GeneratePostgres extends AbstractGenerateCommand {

    @ShellMethod(
            key = "generate-postgres",
            value = "Generate PostgreSQL data")
    public void generate(
            final @ShellOption(help = "JDBC connection URL", defaultValue = "jdbc:postgresql://localhost:5432/postgres") String jdbcUrl,
            final @ShellOption(help = "JDBC connection username", defaultValue = "postgres") String username,
            final @ShellOption(help = "JDBC connection password", defaultValue = "postgres") String password,
            final @ShellOption(help = "Table name", defaultValue = "categories") String table,
            final @ShellOption(help = "Number of records to insert", defaultValue = "1000000") Integer records) {
        super.generate(jdbcUrl, username, password, table, records);
    }

    @Override
    protected String getTableDdl(String tableName) {
        return String.format(
                "CREATE TABLE IF NOT EXISTS %s (id SERIAL PRIMARY KEY, timestamp TIMESTAMP, category VARCHAR(255), value DOUBLE PRECISION)",
                tableName);
    }

}
