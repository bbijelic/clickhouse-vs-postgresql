-- Count records
select count(*) from categories;

-- Count records per category
select category, count(value) from categories group by category;

-- Distinct categories
select distinct(category) FROM categories;

-- Average value grouped by category
select avg(value) FROM categories group by category;

-- Sum value grouped by category
select sum(value) FROM categories group by category;

-- Combined aggregates
select avg(value), sum(value) FROM categories group by category;